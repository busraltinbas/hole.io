using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class PlayerController : MonoBehaviour
{

    public FloatingJoystick floatingJoystick;
    public int Score;
    Rigidbody rb;
    public float Speed;
    public Text Score_Txt;
    public Enemy WinnerEnemy;
   public bool move = true;
    public GameObject Particle_Effect;





    void Start()
    {
        rb=GetComponent<Rigidbody>();
        
    }

    
    void ScoreWrite()
    {
        Score_Txt.text = "SCORE:" + Score;
    }

    void ScaleCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);
    }
    void ScaleLittleCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);

    }
    void ScaleBigCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);

    }

   
    void Player_Move()
    {
        if (move == true)
        {
            transform.position += new Vector3(floatingJoystick.Horizontal, transform.position.y, floatingJoystick.Vertical) * Time.deltaTime * Speed;
        }
    }

    

    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Cube") {

            if (Score > 10)
            {

                col.GetComponent<CubePhysic>().Physic();
                Destroy(col.gameObject, 0.5f);
                col.GetComponent<BoxCollider>().enabled = false;
                ScaleCube();
                Score = Score + 2;
                Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);

            }
        }
        if (col.gameObject.tag == "LittleCube")
        {

            col.GetComponent<CubePhysic>().Physic();
            Destroy(col.gameObject, 0.5f);
            col.GetComponent<BoxCollider>().enabled = false;
            ScaleLittleCube();
            Score++;
            Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);



        }
        if (Score >20)
        {
            if (col.gameObject.tag == "BigCube")
            {

                col.GetComponent<CubePhysic>().Physic();
                Destroy(col.gameObject, 0.5f);
                col.GetComponent<BoxCollider>().enabled = false;
                ScaleBigCube();
                Score = Score + 3;
                Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);

            }

        }
      
    }

    public void Rb()
    {
        if (rb.isKinematic == true)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
        }
    }

  

    void Update()
    {
        Player_Move();
        ScoreWrite();
       

        if (transform.position.x > 10)
            transform.position = new Vector3(10, transform.position.y, transform.position.z);
        if (transform.position.x < -10)
            transform.position = new Vector3(-10, transform.position.y, transform.position.z);
        if (transform.position.z > 28)
            transform.position = new Vector3(transform.position.x, transform.position.y,28);
        if (transform.position.z < -19)
            transform.position = new Vector3(transform.position.x, transform.position.y,-19);

       
    }
}
