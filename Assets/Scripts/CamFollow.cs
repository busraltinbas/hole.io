using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public Transform Target;

    public Transform camTransform;
    
    public Vector3 Offset;
    public Vector3 distance;

    public float SmoothTime = 0.3f;


    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        Offset = camTransform.position - Target.position+distance;
    }

    private void LateUpdate()
    {
        if (Target != null)
        {

            Vector3 targetPosition = Target.position + Offset;
            camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);


            transform.LookAt(Target);
        }
    }
}

