using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCountDown : MonoBehaviour
{
    public Text Time_Txt;
    public GameObject YouWin_Pnl;
    public GameObject YouLose_Pnl;
    public int time = 60;
    public bool control=true;
    public Enemy WinnerEnemy;
    public Text YouWin_Text;
    public Text YouLose_Text;
    

    void Start()
    {
        
        StartCoroutine(TimerTake());
    }


   void YouWin_Write()
    {

        if (WinnerEnemy.MaxPointEnemy == null)
        {
            return;
        }


        int Playerscore = GetComponent<PlayerController>().Score;
        int Maxx = WinnerEnemy.MaxPointEnemy.Score;

        if (time==0 && Playerscore >= Maxx)
        {

            YouWin_Pnl.SetActive(true);
            YouWin_Text.text = "YOU WIN "+ "1." + "Player"  + "        2."+ WinnerEnemy.MaxPointEnemy;
             GetComponent<PlayerController>().move=false;
            
          
        }
        if(time==0 && Maxx > Playerscore)
        {
            
            YouLose_Pnl.SetActive(true);
            YouLose_Text.text ="YOU LOSE " + "1." + WinnerEnemy.MaxPointEnemy+ "       2.Player";
             GetComponent<PlayerController>().move=false;
           
        
        }


    }

  void timee()
    {
        if (time == 0)
        {
            YouWin_Write();
        }
    }

    IEnumerator TimerTake()
    {
        while (control==true)
        {
            Time_Txt.text = "00:" + time;
            yield return new WaitForSeconds(1);
          
            time--;

            
            if (time < 0)
            {
                control = false;
                
            }
            
        }


    }

    private void Update()
    {

        YouWin_Write();
    }
}
