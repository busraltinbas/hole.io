using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPhysic : MonoBehaviour
{
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }
   public void Physics()
    {
        if (rb.isKinematic == true)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
        }
    }
}
