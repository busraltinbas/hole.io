using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyController : MonoBehaviour
{
    public NavMeshAgent meshAgent;
    public int Score;
    public GameObject You_Lose;
    public GameObject Particle_Effect;

    private void Start()
    {
        StartCoroutine(Randm_Move());
    }
    void ScaleCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);
    }
    void ScaleLittleCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);
    }
    void ScaleBigCube()
    {
        transform.localScale += new Vector3(0.02f, 0, 0.02f);
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Cube")
        {
            if (Score > 10)
            {
                col.gameObject.GetComponent<CubePhysic>().Physic();
                Destroy(col.gameObject, 0.5f);
                col.GetComponent<BoxCollider>().enabled = false;
                ScaleCube();
                Score = Score + 2;
                Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);

            }
        }
        if (col.gameObject.tag == "LittleCube")
        {
            col.gameObject.GetComponent<CubePhysic>().Physic();
            Destroy(col.gameObject, 0.5f);
            col.GetComponent<BoxCollider>().enabled = false;
            ScaleLittleCube();
            Score++;
            Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);

        }
        if (col.gameObject.tag == "BigCube")
        {
            if (Score > 20)
            {
                col.gameObject.GetComponent<CubePhysic>().Physic();
                Destroy(col.gameObject, 0.5f);
                col.GetComponent<BoxCollider>().enabled = false;
                ScaleBigCube();
                Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);

            }
        }

        if (col.gameObject.tag == "Player")
        {
            int PlayerScore = col.gameObject.GetComponent<PlayerController>().Score;
            if (PlayerScore < Score)
            {
                col.gameObject.GetComponent<PlayerController>().Rb();
                Destroy(col.gameObject, 0.5f);
                col.GetComponent<BoxCollider>().enabled = false;
                Score = Score + PlayerScore;
                You_Lose.SetActive(true);
                
                
            }
        }
        
    }

   
    IEnumerator Randm_Move()
    {
       
        
            while (true)
            {
                Vector3 rndm = new Vector3(Random.Range(-11, 13), 0, Random.Range(-24, 32));
                meshAgent.SetDestination(rndm);
                yield return new WaitForSeconds(1);
            }
        
    }
}
